import java.time.Clock

import com.google.inject.AbstractModule
import com.google.inject.name.Names
import domain.repository.PicturePropertyRepository
import infrastructure.actor.{ActorScheduler, ConvertPictureActor}
import infrastructure.repository.PicturePropertyRepositoryImpl
import play.api.libs.concurrent.AkkaGuiceSupport
import play.api.{Configuration, Environment}

class Module(
  environment: Environment,
  configuration: Configuration
) extends AbstractModule with AkkaGuiceSupport {

  val redisHost = configuration.get[String]("mojipic.redis.host")
  val redisPost = configuration.get[Int]("mojipic.redis.port")

  def configure() = {
    bind(classOf[Clock]).toInstance(Clock.systemDefaultZone())
    bind(classOf[PicturePropertyRepository]).to(classOf[PicturePropertyRepositoryImpl])
    bindActor[ConvertPictureActor]("convert-picture-actor")
    bind(classOf[ActorScheduler])
      .annotatedWith(Names.named("convert-picture-actor"))
      .to(classOf[ActorScheduler])
      .asEagerSingleton()
  }
}