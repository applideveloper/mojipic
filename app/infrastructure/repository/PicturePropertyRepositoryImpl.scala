package infrastructure.repository

import java.time.LocalDateTime

import com.google.common.net.MediaType
import domain.entity.{PictureId, PictureProperty, TwitterId}
import domain.repository.PicturePropertyRepository
import scalikejdbc._

import scala.concurrent.Future
import scala.util.Try

class PicturePropertyRepositoryImpl extends PicturePropertyRepository {
  /**
    * 画像のプロパティを保存する
    *
    * @param value 画像のプロパティの値
    * @return Future.success(PictureId) 新しく割り当てられた画像ID
    */
  override def create(value: PictureProperty.Value): Future[PictureId] =
    Future.fromTry(Try {
      using(DB(ConnectionPool.borrow())) { db =>
        db.localTx { implicit session =>
          val sql =
            sql"""INSERT INTO picture_properties (
                 | status,
                 | twitter_id,
                 | file_name,
                 | content_type,
                 | overlay_text,
                 | overlay_text_size,
                 | original_filepath,
                 | converted_filepath,
                 | created_time
                 | ) VALUES (
                 | ${value.status.value},
                 | ${value.twitterId.value},
                 | ${value.fileName},
                 | ${value.contentType.toString},
                 | ${value.overlayText},
                 | ${value.overlayTextSize},
                 | ${value.originalFilepath.getOrElse(null)},
                 | ${value.convertedFilepath.getOrElse(null)},
                 | ${value.createdTime}
                 | )
              """.stripMargin
          PictureId(sql.updateAndReturnGeneratedKey().apply())
        }
      }
    })

  /**
    * 画像のプロパティを読み込む
    *
    * @param pictureId 画像ID
    * @return Future.successfull(PictureProperty)
    */
  def find(pictureId: PictureId): Future[PictureProperty] =
    Future.fromTry(Try {
      using(DB(ConnectionPool.borrow())) { db =>
        db.readOnly { implicit session =>
          val sql =
            sql"""SELECT
                 | picture_id,
                 | status,
                 | twitter_id,
                 | file_name,
                 | content_type,
                 | overlay_text,
                 | overlay_text_size,
                 | original_filepath,
                 | converted_filepath,
                 | created_time
                 | FROM picture_properties WHERE picture_id = ${pictureId.value}
              """.stripMargin
          sql.map(resultSetToPictureProperty).single().apply()
            .getOrElse(throw new RuntimeException(s"Picture is notfound. PictureId: ${pictureId.value}"))
        }
      }
    })

  private[this] def resultSetToPictureProperty(rs: WrappedResultSet): PictureProperty = {
    val value =
      PictureProperty.Value(
        PictureProperty.Status.parse(rs.string("status")).get,
        TwitterId(rs.long("twitter_id")),
        rs.string("file_name"),
        MediaType.parse(rs.string("content_type")),
        rs.string("overlay_text"),
        rs.int("overlay_text_size"),
        rs.stringOpt("original_filepath"),
        rs.stringOpt("converted_filepath"),
        rs.localDateTime("created_time")
      )
    PictureProperty(PictureId(rs.long("picture_id")), value)
  }

  /**
    * 画像を更新する
    *
    * @param pictureId 画像ID
    * @param value     値
    * @return Future.successful() 更新に成功した
    */
  override def update(pictureId: PictureId, value: PictureProperty.Value): Future[Unit] =
    Future.fromTry(Try {
      using(DB(ConnectionPool.borrow())) { db =>
        db.localTx { implicit session =>
          val sql =
            sql"""UPDATE picture_properties SET
                 | status =  ${value.status.value},
                 | twitter_id = ${value.twitterId.value},
                 | file_name = ${value.fileName},
                 | content_type = ${value.contentType.toString},
                 | overlay_text = ${value.overlayText},
                 | overlay_text_size = ${value.overlayTextSize},
                 | original_filepath = ${value.originalFilepath.getOrElse("")},
                 | converted_filepath = ${value.convertedFilepath.getOrElse("")},
                 | created_time = ${value.createdTime}
                 | WHERE picture_id = ${pictureId.value}""".stripMargin
          sql.update().apply()
          ()
        }
      }
    })

  /**
    * 投稿者のTwitter IDと最後に読み込まれた作成日時から画像のプロパティを読み込む
    *
    * @param twitterId       投稿者のTwitter ID
    * @param lastCreatedTime 最後に読み込まれた作成日時
    * @return Future.successfull(Seq(PictureProperty)) 読み込みに成功した
    */
  def findAllByTwitterIdAndDateTime(twitterId: TwitterId, toDateTime: LocalDateTime): Future[Seq[PictureProperty]] =
    Future.fromTry(Try {
      using(DB(ConnectionPool.borrow())) { db =>
        db.readOnly { implicit session =>
          val sql =
            sql"""SELECT
                 | picture_id,
                 | status,
                 | twitter_id,
                 | file_name,
                 | content_type,
                 | overlay_text,
                 | overlay_text_size,
                 | original_filepath,
                 | converted_filepath,
                 | created_time
                 | FROM picture_properties
                 | WHERE twitter_id = ${twitterId.value} AND created_time > ${toDateTime} ORDER BY created_time DESC
              """.stripMargin
          sql.map(resultSetToPictureProperty).list().apply()
        }
      }
    })

  /**
    * 最後に読み込まれた作成日時から画像のプロパティを読み込む
    *
    * @param toDateTime 最後に読み込まれた作成日時
    * @return Future.successful(Seq(PictureProperty)) 読み込みに成功した
    */
  def findAllByDateTime(toDateTime: LocalDateTime): Future[Seq[PictureProperty]] =
    Future.fromTry(Try {
      using(DB(ConnectionPool.borrow())) { db =>
        db.readOnly { implicit session =>
          val sql =
            sql"""SELECT
                 | picture_id,
                 | status,
                 | twitter_id,
                 | file_name,
                 | content_type,
                 | overlay_text,
                 | overlay_text_size,
                 | original_filepath,
                 | converted_filepath,
                 | created_time
                 | FROM picture_properties WHERE created_time > ${toDateTime} ORDER BY created_time DESC
              """.stripMargin
          sql.map(resultSetToPictureProperty).list().apply()
        }
      }
    })
}
